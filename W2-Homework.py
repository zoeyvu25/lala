# exercice1
u, v, x, y, z=29, 12, 10, 4, 3

a1 = u/v
a2 = (u==v)
a3 = u%x
a4 = (x >= y)
a5 = u + 5
a6 = u % z
a7 = (v > x and y < z)
a8 = x**z
a9 = x // z

print('a1=', a1)
print('t=', a2)
print('a3=', a3)
print('t=', a4)
print('a5=', a5)
print('a6=', a6)
print('t=', a7)
print('a8=', a8)
print('a9=', a9)

# exercice2
s = "Hi John, welcome to python programming for beginner!"

print('Check the string "python" is in s or not:', "python" in s)
s2 = s.split(" ")[1]
s1 = s2[0:4]
print ('s1=', s1)

s3 = s.split(" ")
s4 = len(s3)
print ('Words in s=', s4)

# exercise 3
import math
r = 5
C = r * 2 * math.pi
S = r * r * math.pi

print('Perimeter: ', round(C,2))
print('Area: ', round(S,2))

# exercise 5
I = [23, 4.3, 4.2, 31, 'python', 1, 5.3, 9, 1.7]

I.remove('python')
print ('Remove "Python" in the list', I)

I.sort()
print ('Sorted in Ascending: ', I)

I.sort(reverse=True)
print ('Sorted in Descending: ', I)

print ('Number 4.2 is in I or not: ', 4.2 in I)



