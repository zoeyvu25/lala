
# coding: utf-8

# In[11]:


#Exercise 1: Sets

A = {1, 2, 3, 4, 5, 7}
B = {2, 4, 5, 9, 12, 24}
C = {2, 4, 8}


# In[13]:


for num in C:
    print(num)


# In[19]:


#add each element to A and B
A.add(8)
A.add(2)
A.add(4)

B.add(8)
B.add(2)
B.add(4)


# In[20]:


#print A and B after adding the elements
print (A)
print (B)


# In[24]:


#intersection of A and B
 A & B 


# In[25]:


#union of A and B
A | B 


# In[28]:


#elements in A not in B
A - B


# In[29]:


#length of A and B
print(len(A))
print(len(B))


# In[30]:


#maximum value and minimum value of A union B
print(max(A|B))
print(min(A|B))


# In[31]:


#Exercise 2: Tuples
t = (1 , "python", [2, 3], (4,5))


# In[32]:


#unpack t
a, b, c, d = t
print (a)
print (b)
print (c)
print (d)


# In[33]:


e, f = c
print (e)
print (f)
g, h = d
print (g)
print (h)


# In[34]:


#last element of t
print (t[-1])


# In[41]:


#add to t a list 
new = t + ([2,3],)
t=new
print (t)


# In[48]:


# check whether list is duplicated in t
count = t.count([2, 3])
print ('list[2,3] is duplicated in t:', count)


# In[50]:


#convert typle to list
newlist = list(t)


# In[51]:


#remove list from t
newlist.remove([2,3])


# In[52]:


#Exercise 3: Dictionaries
#3.1
dic1={1:10, 2:20}
dic2={3:30, 4:40}
dic3={5:50, 6:60}
newdic={}
newdic.update(dic1)
newdic.update(dic2)
newdic.update(dic3)
print(newdic)


# In[55]:


dic={}
for x in range (1,15):
    dic[x]=x*x
print (dic)


# In[60]:


#3.2
dic4={'a': 1, 'b': 4, 'c': 2}


# In[77]:


sorted_dict = (sorted(dic4.items(), key=lambda kv: kv[1]))
print (sorted_dict)


# In[83]:


sorted_keys = dict(sorted_dict)
print(sorted_keys)
sorted_keys.keys()

